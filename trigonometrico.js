class Sen {
  constructor ( dato = 0 ) {
    this._dato = dato;
  }

  set dato ( valor ) {
    this._dato = valor;
  }

  get seno() {
    return Math.sin(this._dato)
  }
}

module.exports = Sen