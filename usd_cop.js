class Usd_cop {
  constructor ( dato = 0 ) {
    this._dato = dato;
  }

  set dato ( valor ) {
    this._dato = valor;
  }

  get usd_cop() {
    return ((this._dato) / 3347)
  }
}

module.exports = Usd_cop